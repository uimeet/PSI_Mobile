import React from 'react';

import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

class MsgBox extends React.Component {

  render() {
    const { info, open, onOK, ...other } = this.props;

    return (
      <Dialog open={open} {...other}>
        <DialogContent>
          <DialogContentText>
            {info}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={onOK} color="primary" autoFocus>
            确定
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
};

export default MsgBox;
