import React from 'react';
import { connect } from 'dva';

import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import TextField from '@material-ui/core/TextField';
import RightArrowIcon from '@material-ui/icons/KeyboardArrowRight'
import IconButton from '@material-ui/core/IconButton';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import CustomerCategoryDialog from './CustomerCategoryDialog';
import Slide from '@material-ui/core/Slide';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';

const styles = {
  appBar: {
    position: 'static',
  },
  flex: {
    flex: 1,
  },
  form: {
    padding: 10,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class CustomerQueryDialog extends React.Component {
  state = {
    categoryDialogOpen: false,
    selectedTab: 0,
    code: '',
    name: '',
    mobile: '',
    tel: '',
    address: '',
    contact: '',
    qq: ''
  }

  onCategroySelect = ({ text, id }) => {
    const { dispatch, qc } = this.props;
    dispatch({
      type: 'customerList/saveQueryCondition', payload: {
        qc: { ...qc, categoryId: id, categoryName: text },
      }
    });
  }

  onCategoryListItemClick = () => {
    const { dispatch } = this.props;
    dispatch({ type: 'customerCategoryList/queryCategoryListWithAllCategory', payload: {} });

    this.setState({ categoryDialogOpen: true });
  }

  tabChange = (event, value) => {
    this.setState({ selectedTab: value });
  }

  /**
   * 单击查询按钮
   */
  onButtonOK = () => {
    const { onOK } = this.props;
    const { dispatch, qc: { categoryId, categoryName } } = this.props;
    const { code, name, mobile, tel, address, contact, qq } = this.state;
    const qc = { categoryId, categoryName, code, name, mobile, tel, address, contact, qq };
    // 同步qc到Model
    dispatch({
      type: 'customerList/saveQueryCondition', payload: {
        qc
      }
    });

    // 查询
    dispatch({
      type: 'customerList/queryCustomerList', payload: {
        page: 1,
        qc
      }
    });

    onOK();
  }

  /**
   * 重置查询条件
   */
  onReset = () => {
    this.setState({
      code: '',
      name: '',
      mobile: '',
      tel: '',
      address: '',
      contact: '',
      qq: ''
    });
    const { dispatch } = this.props;
    dispatch({
      type: 'customerList/saveQueryCondition', payload: {
        qc: {
          categoryId: '-1',
          categoryName: '[所有分类]',
          code: '',
          name: '',
          mobile: '',
          tel: '',
          address: '',
          contact: '',
          qq: ''
        }
      }
    });
  }

  render() {
    const { classes, open, onOK, onCancel, qc: { categoryName }, ...other } = this.props;
    const { categoryDialogOpen, selectedTab, code, name, mobile, tel, address, contact, qq } = this.state;

    return (
      <Dialog fullScreen open={open} {...other} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton className={classes.menuButton} color="inherit" onClick={onCancel}>
              <CloseIcon />
            </IconButton>
            <Typography variant="title" color="inherit" className={classes.flex}>
              查询条件
            </Typography>
            <Button color="inherit" onClick={this.onButtonOK}>
              查询
            </Button>
          </Toolbar>
        </AppBar>

        <form className={classes.form}>
          <Tabs
            value={selectedTab}
            indicatorColor="primary"
            textColor="primary"
            onChange={this.tabChange}
          >
            <Tab fullWidth label='常用' value={0}>
            </Tab>
            <Tab label='更多条件' value={1}></Tab>
          </Tabs>
          {
            selectedTab === 0 &&
            <div>
              <FormControl fullWidth margin="none">
                <InputLabel>客户分类</InputLabel>
                <Input onClick={this.onCategoryListItemClick}
                  value={categoryName} readOnly
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton
                        onClick={this.onCategoryListItemClick}
                      >
                        <RightArrowIcon />
                      </IconButton>
                    </InputAdornment>
                  }
                />
              </FormControl>
              <TextField type='text' label='客户编码' fullWidth margin="none"
                value={code} onChange={(e) => { this.setState({ code: e.target.value }) }}></TextField>
              <TextField type='text' label='客户名称' fullWidth margin="none"
                value={name} onChange={(e) => { this.setState({ name: e.target.value }) }}></TextField>
              <TextField type='text' label='手机' fullWidth margin="none"
                value={mobile} onChange={(e) => { this.setState({ mobile: e.target.value }) }}></TextField>
              <TextField type='text' label='固话' fullWidth margin="none"
                value={tel} onChange={(e) => { this.setState({ tel: e.target.value }) }}></TextField>
            </div>
          }
          {
            selectedTab === 1 &&
            <div>
              <TextField type='text' label='地址' fullWidth margin="none"
                value={address} onChange={(e) => { this.setState({ address: e.target.value }) }}></TextField>
              <TextField type='text' label='联系人' fullWidth margin="none"
                value={contact} onChange={(e) => { this.setState({ contact: e.target.value }) }}></TextField>
              <TextField type='text' label='QQ' fullWidth margin="none"
                value={qq} onChange={(e) => { this.setState({ qq: e.target.value }) }}></TextField>
            </div>
          }
        </form>
        <Button onClick={this.onReset} color="primary">
          重置查询条件
        </Button>
        <CustomerCategoryDialog open={categoryDialogOpen}
          onClose={() => { this.setState({ categoryDialogOpen: false }) }}
          onCategroySelect={this.onCategroySelect} />
      </Dialog>
    );
  }
}

CustomerQueryDialog.propTypes = {
};

function mapStateToProps(state) {
  const { qc } = state.customerList;

  return {
    qc
  };
}

export default connect(mapStateToProps)(withStyles(styles)(CustomerQueryDialog));
