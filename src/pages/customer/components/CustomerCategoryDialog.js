import React from 'react';
import { connect } from 'dva';

import { withStyles } from '@material-ui/core/styles';

import Slide from '@material-ui/core/Slide';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import Dialog from '@material-ui/core/Dialog';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const styles = {
  appBar: {
    position: 'static',
  },
  flex: {
    flex: 1,
  },
  form: {
    padding: 10,
  },
};

function Transition(props) {
  return <Slide direction="up" {...props} />;
}


class CustomerCategoryDialog extends React.Component {
  onClick = (event) => {
    const id = event.currentTarget.getAttribute('data-id');
    const text = event.currentTarget.getAttribute('data-name');

    const { onCategroySelect, onClose } = this.props;
    onClose();
    onCategroySelect({ id: id, text: text });
  }

  render() {

    const { classes, open, onCategroySelect, onClose, list, ...other } = this.props;

    return (
      <Dialog onClose={onClose} open={open} {...other} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <Typography variant="title" color="inherit" className={classes.flex}>
              选择客户分类
            </Typography>
          </Toolbar>
        </AppBar>
        <List>
          {
            list && list.map((it, index) => {
              return <ListItem key={index} button divider onClick={this.onClick}
                data-id={it.id} data-name={it.name}>
                <ListItemText>{it.name}</ListItemText>
              </ListItem>
            })
          }
        </List>
      </Dialog>
    );
  }
}

CustomerCategoryDialog.propTypes = {
};

function mapStateToProps(state) {
  const { list } = state.customerCategoryList;
  return {
    list
  };
}

export default connect(mapStateToProps)(withStyles(styles)(CustomerCategoryDialog));
