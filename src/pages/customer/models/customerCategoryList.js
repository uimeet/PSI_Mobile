import * as customerService from '../services/customerService';

export default {
  namespace: 'customerCategoryList',
  state: {
    // 客户分类List
    list: [],

    entity: {
      id: '',
      code: '',
      name: '',
      psId: '',
      psName: '',
      canDelete: false,
    },

    // 价格体系List
    psList: [],
  },

  reducers: {
    save(state, { payload: { list } }) {
      return { ...state, list };
    },

    savePSList(state, { payload: { psList } }) {
      return { ...state, psList };
    },

    saveEntity(state, { payload: { entity } }) {
      return { ...state, entity };
    },
  },

  effects: {
    *queryCategoryListWithAllCategory({ payload }, { call, put }) {
      const { data: list } = yield call(customerService.queryCategoryListWithAllCategory);

      yield put({
        type: 'save',
        payload: {
          list
        },
      });
    },
    *queryCategoryList({ payload }, { call, put }) {
      const { data: list } = yield call(customerService.queryCategoryList);

      yield put({
        type: 'save',
        payload: {
          list
        },
      });
    },
    *queryPriceSystemList({ payload }, { call, put }) {
      const { data: psList } = yield call(customerService.queryPriceSystemList);

      yield put({
        type: 'savePSList',
        payload: {
          psList
        },
      });
    },
    *queryCustomerCategory({ payload: { categoryId } }, { call, put }) {
      const { data: entity } = yield call(customerService.queryCustomerCategory, { categoryId });

      yield put({
        type: 'saveEntity',
        payload: {
          entity
        },
      });
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      // 监听url变化
      return history.listen(({ pathname, query }) => {
      });
    },
  },
};
