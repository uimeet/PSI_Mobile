import React from 'react';
import { connect } from 'dva';
import router from 'umi/router';

import { getPSITokenId } from '../../../utils/tokenId';

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';

import LeftArrowIcon from '@material-ui/icons/KeyboardArrowLeft'
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider } from '@material-ui/core/styles';
import mainTheme from '../../../theme/mainTheme';
import LinearProgress from '@material-ui/core/LinearProgress';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import styles from '../../../utils/pageLayout';

class SOBillDetailPage extends React.Component {
  state = {
    selectedTab: 0,
  }

  constructor(props) {
    super(props);

    if (!getPSITokenId()) {
      router.replace('/auth/login');
    }
  }

  componentDidMount = () => {
  }

  tabChange = (event, value) => {
    this.setState({ selectedTab: value });
  }

  render() {
    const { classes, loading, entity: bill } = this.props;
    const { selectedTab } = this.state;

    return (
      <MuiThemeProvider theme={mainTheme}>
        <CssBaseline />
        <AppBar position='static'>
          <Toolbar>
            <IconButton className={classes.menuButton} onClick={() => { router.goBack() }}>
              <LeftArrowIcon color='inherit' />
            </IconButton>
            <Typography variant="title" color="inherit">
              销售订单详情
              </Typography>
          </Toolbar>
        </AppBar>
        <LinearProgress color='secondary' hidden={!loading} />
        <Tabs
          value={selectedTab}
          indicatorColor="primary"
          textColor="primary"
          onChange={this.tabChange}
        >
          <Tab label='主要' value={0}></Tab>
          <Tab label='金额' value={1}></Tab>
          <Tab label='客户' value={2}></Tab>
          <Tab label='商品明细' value={3}></Tab>
        </Tabs>
        {
          selectedTab === 0 && bill &&
          <Table>
            <TableBody>
              <TableRow><TableCell>单号</TableCell><TableCell>{bill.ref}</TableCell></TableRow>
              <TableRow><TableCell>订单状态</TableCell><TableCell>{bill.billStatus}</TableCell></TableRow>
              <TableRow><TableCell>交货日期</TableCell><TableCell>{bill.dealDate}</TableCell></TableRow>
              <TableRow><TableCell>交货地址</TableCell><TableCell>{bill.dealAddress}</TableCell></TableRow>
              <TableRow><TableCell>组织机构</TableCell><TableCell>{bill.orgFullName}</TableCell></TableRow>
              <TableRow><TableCell>业务员</TableCell><TableCell>{bill.bizUserName}</TableCell></TableRow>
              <TableRow><TableCell>备注</TableCell><TableCell>{bill.billMemo}</TableCell></TableRow>
            </TableBody>
          </Table>
        }
        {
          selectedTab === 1 && bill &&
          <Table>
            <TableBody>
              <TableRow><TableCell>收款方式</TableCell><TableCell>{bill.receivingType}</TableCell></TableRow>
              <TableRow><TableCell>销售额</TableCell><TableCell>{bill.goodsMoney}</TableCell></TableRow>
              <TableRow><TableCell>税金</TableCell><TableCell>{bill.tax}</TableCell></TableRow>
              <TableRow><TableCell>价税合计</TableCell><TableCell>{bill.moneyWithTax}</TableCell></TableRow>
            </TableBody>
          </Table>
        }
        {
          selectedTab === 2 && bill &&
          <Table>
            <TableBody>
              <TableRow><TableCell>客户</TableCell><TableCell>{bill.customerName}</TableCell></TableRow>
              <TableRow><TableCell>联系人</TableCell><TableCell>{bill.contact}</TableCell></TableRow>
              <TableRow><TableCell>电话</TableCell><TableCell>{bill.tel}</TableCell></TableRow>
              <TableRow><TableCell>传真</TableCell><TableCell>{bill.fax}</TableCell></TableRow>
            </TableBody>
          </Table>
        }
        {
          selectedTab === 3 && bill && bill.items &&
          <List>
            {
              bill.items.map((it, index) => {
                return <ListItem key={index} divider>
                  <ListItemText>
                    <div>&emsp;&emsp;编码:{it.goodsCode}</div>
                    <div>商品名称:{it.goodsName}</div>
                    <div>规格型号:{it.goodsSpec}</div>
                    <div>销售数量:{it.goodsCount}{it.unitName}</div>
                    <div>&emsp;&emsp;单价:{it.goodsPrice}元</div>
                    <div>&emsp;&emsp;金额:{it.goodsMoney}元</div>
                    <div>&emsp;&emsp;税率:{it.taxRate}% / 税金:{it.tax}</div>
                    <div>价税合计:{it.moneyWithTax}元</div>
                    <div>&emsp;&emsp;备注:{it.memo}</div>
                  </ListItemText>
                </ListItem>
              })
            }
          </List>
        }

      </MuiThemeProvider >
    );
  }
}

SOBillDetailPage.propTypes = {
};

function mapStateToProps(state) {
  const { entity } = state.sobillList;

  return {
    entity, loading: state.loading.models.sobillList
  };
}


export default connect(mapStateToProps)(withStyles(styles)(SOBillDetailPage));
