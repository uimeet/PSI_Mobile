import request from '../../../utils/request';

import {getPSITokenId} from '../../../utils/tokenId';

export function querySOBillList({ page = 1 }) {
    var formData = new FormData();
    formData.append("tokenId", getPSITokenId());
    formData.append("page", page);

    return request('/web/API/SOBill/sobillList', {
        credentials: 'include',
        method: "POST",
        body: formData,
    });
}

export function querySOBillInfo({ id }) {
  var formData = new FormData();
  formData.append("tokenId", getPSITokenId());
  formData.append("id", id);

  return request('/web/API/SOBill/sobillInfo', {
      credentials: 'include',
      method: "POST",
      body: formData,
  });
}
