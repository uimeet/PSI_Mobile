# PSI Mobile

#### 项目介绍
PSI ( https://gitee.com/crm8000/PSI ) 移动端

#### 技术架构

Material-UI https://material-ui.com/

UmiJS https://umijs.org/

DVA https://github.com/dvajs/dva

#### 开发环境
1. IDE - VS Code

2. nodejs

安装完nodejs可以npm的仓库设置为国内淘宝镜像
`npm config set registry https://registry.npm.taobao.org`

#### 开发
> `git clone https://gitee.com/crm8000/PSI_Mobile.git`
>
> `cd PSI_Mobile`
>
> `npm install`
>
> `npm start`

首次安装的的时候，可以把杀毒软件等监控本地文件的关闭，以防止npm install失败。

#### 生成部署文件
> `npm run build`
>
> 参考PSI部署脚本：https://gitee.com/crm8000/PSI/blob/master/doc/05%20Script/build_All.sh

#### 部署参考文档
> PSI_Mobile的底层使用了UmiJS，这是它的部署参考文档：
> 
> https://umijs.org/zh/guide/deploy.html

#### ES参考书
> http://es6.ruanyifeng.com/