export default {
    "proxy": {
      "/web/API": {
        "target": "http://localhost/web/API",
        "changeOrigin": true,
        "pathRewrite": { "^/web/API" : "" }
      }
    },
  }